package edu.ucsd.cse110.library;
import java.time.LocalDate;

import edu.ucsd.cse110.library.rules.*;


public class LibraryFacade {
	public void checkoutPublication(Member m, Publication pub, LocalDate d){
		pub.checkout(m,d);
	}
	
    public void checkoutPublication(Member m, Publication pub){
    	LocalDate checkoutDate = LocalDate.now();
    	checkoutPublication(m,pub,checkoutDate);
    }
    public void returnPublication(Publication pub, LocalDate d){
    	pub.pubReturn( d);
    }
    public void returnPublication(Publication pub){
    	LocalDate returnDate = LocalDate.now();
    	returnPublication(pub, returnDate);
    	
    }
    public double getFee(Member m){
    	return m.getDueFees();
    }
    public boolean hasFee(Member m){
    	return ( m.getDueFees() != 0) ? true: false;
    }
    
}
